<?php

use Bitrix\Main\Application;

require_once Application::getDocumentRoot() . '/vendor/autoload.php';
require_once __DIR__ . '/js_extensions.php';
