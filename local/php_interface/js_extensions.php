<?php

use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;

$taskTemplates = [
    'taskView' => ltrim(Option::get('intranet', 'path_user', '', SITE_ID), '/')
        . 'tasks/task/view/#TASK_ID#/'
];

$variables = [];

if (CComponentEngine::parseComponentPath('/', $taskTemplates, $variables) == 'taskView') {
    CJSCore::RegisterExt('task_extension',
        [
            'js' => '/local/js/extensions/task/script.js',
            'lang' => '/local/lang/' . LANGUAGE_ID . '/extensions/task.php',
        ]
    );

    CJSCore::Init('task_extension');

    $asset = Asset::getInstance();
    $asset->addString('<script>BX.ready(function () {
        const taskExtension = new BX.TaskExtension()
        taskExtension.createCloneButton() 
    })</script>');
}