BX.namespace('TasksExtension')

BX.TaskExtension = BX.Tasks.Util.Widget.extend({
  sys: {
    code: 'task-clone'
  },
  methods: {
    construct: function () {
      this.callConstruct(BX.Tasks.Util.Widget);
    },
    /**
     * Создание кнопки "Клонировать" в меню "Еще" в задаче
     */
    createCloneButton: function () {
      const cloneButton = BX.create('span', {
        attrs: {
          className: 'menu-popup-item menu-popup-item-clone',
        },
        html: `<span title="${BX.message('TASK_EXTENSION_CLONE')}" class="menu-popup-item menu-popup-item-copy ">
            <span class="menu-popup-item-icon"></span><span class="menu-popup-item-text">${BX.message('TASK_EXTENSION_CLONE')}</span></span>`
      })

      BX.bind(cloneButton, 'click', this.passCtx(this.clone));

      const body = document.querySelector('body')

      body.addEventListener('DOMNodeInserted', function (e) {
        if (e.target.lastElementChild && e.target.lastElementChild.classList &&
          e.target.lastElementChild.classList.value === 'menu-popup-items') {
          const taskMorePopupItems = e.target.querySelector('.menu-popup-items')

          taskMorePopupItems.appendChild(cloneButton)
        }
      }, false)

    },
    /**
     * Открытие окна новой подзадачи
     */
    clone: function () {
      const taskPath = window.location.pathname.split('/')
      taskPath.pop()
      const taskId = taskPath.pop()
      taskPath.pop()

      const subTaskPath = taskPath.join('/') + `/edit/0/?PARENT_ID=${taskId}/SOURCE=view`

      const taskTitleElement = document.querySelector('#pagetitle')
      const taskDescriptionElement = document.querySelector('#task-detail-description')

      if (taskTitleElement && taskDescriptionElement) {
        const taskTitle = taskTitleElement.textContent;
        const taskDescription = taskDescriptionElement.textContent;

        BX.SidePanel.Instance.closeAll()
        BX.SidePanel.Instance.open(subTaskPath);

        this.copyTaskData({
          title: taskTitle,
          description: taskDescription,
        })
      }
    },
    /**
     * Копирование данных из задачи в подзадачу
     * @param task
     */
    copyTaskData: function (task) {
      if (task.title && task.description) {
        const openSliders = BX.SidePanel.Instance.getOpenSliders();

        setTimeout(function () {
          if (openSliders.length > 0) {
            const slider = openSliders[0]

            const panelIframe = slider.iframe;
            const panelIframeDocument = panelIframe.contentWindow.document;

            const newTaskTitleInput = panelIframeDocument.querySelector('input[data-bx-id="task-edit-title"]')
            const descriptionIframe = panelIframeDocument.querySelector('.bx-editor-iframe')
            const descriptionIframeBody = descriptionIframe.contentWindow.document.body;

            newTaskTitleInput.value = task.title
            descriptionIframeBody.innerHTML = task.description
          }
        }, 400);
      }
    },
  }
})